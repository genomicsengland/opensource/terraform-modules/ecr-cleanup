terraform {
  required_version = ">= 1.2.0"
}

resource "aws_ecr_lifecycle_policy" "repository" {
  repository = var.repository
  policy     = jsonencode({ rules = concat(local.protected, local.rules, local.default_rules) })

  lifecycle {
    precondition {
      condition     = alltrue([for value in var.prefix_ttls : (value.days >= var.minimum_ttl)])
      error_message = "minimum_ttl must not exceed prefix_ttls"
    }
  }
}
