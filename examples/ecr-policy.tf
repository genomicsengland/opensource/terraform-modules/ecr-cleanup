resource "aws_ecr_repository" "example" {
  name     = "example"
}

module "lifecycle_policy" {
  source             = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/ecr-cleanup?ref=2023.06.23"
  repository         = aws_ecr_repository.example.name
  protected_prefixes = ["latest-"]  # keep everything that starts with "latest-"
  prefix_ttls = [
    { prefix = "prod-", days = 90 },  # keep prod- images for 90 days
    { prefix = "test-", days = 60 },  # keep test- images for 60 days
    { prefix = "dev-", days = 30 },  # keep dev- images for 30 days
    { prefix = "0", days = 30 },  # keep versioned images (starting with 0-9) for 30 days
    { prefix = "1", days = 30 },
    { prefix = "2", days = 30 },
    { prefix = "3", days = 30 },
    { prefix = "4", days = 30 },
    { prefix = "5", days = 30 },
    { prefix = "6", days = 30 },
    { prefix = "7", days = 30 },
    { prefix = "8", days = 30 },
    { prefix = "9", days = 30 },
  ]
  minimum_ttl  = 14
  untagged_ttl = 1
}
