variable "repository" {
  description = "name of the repository (in the current account)"
  type        = string
}

variable "protected_prefixes" {
  description = "list of image tag prefixes whose images will never be deleted"
  type        = list(string)
  default     = ["latest"]
}

variable "prefix_ttls" {
  description = "list of (tag prefix, ttl in days) pairs; must be ordered by decreasing priority"
  type = list(object({
    prefix = string
    days   = number
  }))
  default = []
  validation {
    condition     = alltrue([for value in var.prefix_ttls : (value.days >= 1)])
    error_message = "value for days must be positive integer"
  }
  validation {
    condition = alltrue([
      for idx in range(max(length(var.prefix_ttls) - 1, 0)) : (var.prefix_ttls[idx].days >= var.prefix_ttls[idx + 1].days)
    ])
    error_message = "value for days must not increase with decreasing priority"
  }
}

variable "minimum_ttl" {
  description = "minimum lifetime of tagged images, in days"
  type        = number
  default     = 14
  validation {
    condition     = var.minimum_ttl >= 1
    error_message = "minimum_ttl must be positive integer"
  }
}

variable "untagged_ttl" {
  description = "minimum lifetime of untagged images, in days"
  type        = number
  default     = 1
  validation {
    condition     = var.untagged_ttl >= 1
    error_message = "untagged_ttl must be positive integer"
  }
}