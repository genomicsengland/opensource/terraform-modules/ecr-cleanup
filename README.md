# ECR cleanup

This module provides a simplified interface to
[AWS ECR Repository Lifecycle Policy](https://docs.aws.amazon.com/AmazonECR/latest/userguide/LifecyclePolicies.html).

It is intentionally restrictive in the policies it can generate:
* count based expiry is not supported, only age based expiry (see below for how to emulate count based rules)
* no multi-tag rules, i.e. rules that require two or more tags to be present to match
* age can only decrease with decreasing priority

As a result, the generated policy is free of surprises: every rule can now be read as a guaranteed minimum survival time.

## Input parameters
#### `repository`
The repository to apply the rules to in the current account.
#### `protected_prefixes`
List of tag prefixes; any image with a tag that starts with any of the prefixes is preserved forever.  
Default: `["latest"]`
#### `prefix_ttls`
List of `{prefix, days}` objects. Any image with a tag that starts with `prefix` will be preserved for `days`. The first
matching rule applies. The `days` parameter must not increase in later rules.
Example:
```hcl
prefix_ttl = [
  { prefix = "prod-", days = 180 },
  { prefix = "test-", days = 30 }
]
```
Keep any image with a `prod-*` tag for 180 days, images with only a `test-*` tag for 30 days.  
Default: `[]`
#### `minimum_ttl`
Expire tagged images that are not covered by other rules after `minimum_ttl` days.   
Default: 14
#### `untagged_ttl`
Expire untagged images after `untagged_ttl` days.   
Default: 1

## Emulating count based policy
A count based policy can be emulated as follows. Let's assume you want to preserve the 3 most recently deployed prod
images. To do this, use tags `latest-prod-0`, `latest-prod-1`, `latest-prod-2` and add them to the protected prefixes
list: `["latest-prod-"]`
Whenever a new image is deployed to prod, re-tag the latest images:
1. image with tag `latest-prod-1` is tagged with `latest-prod-2`
2. image with tag `latest-prod-0` is tagged with `latest-prod-1`
3. the new image is tagged with `latest-prod-0`

As tags are unique, we end up with exactly the 3 most recent images, and thanks to the protected prefixes, they will not
be deleted by other rules. For this to work, tag immutability has to be (temporarily) disabled.
