locals {
  protected = [for idx, prefix in var.protected_prefixes :
    {
      rulePriority = 100 + idx
      description  = "Never expire '${prefix}' images"
      selection = {
        tagStatus     = "tagged"
        tagPrefixList = [prefix]
        countType     = "sinceImagePushed"
        countUnit     = "days"
        countNumber   = 36500
      }
      action = {
        type = "expire"
      }
    }
  ]
  rules = [for idx, val in var.prefix_ttls :
    {
      rulePriority = 500 + idx
      description  = "Expire '${val.prefix}' images older than ${val.days} days"
      selection = {
        tagStatus     = "tagged"
        tagPrefixList = [val.prefix]
        countType     = "sinceImagePushed"
        countUnit     = "days"
        countNumber   = val.days
      }
      action = {
        type = "expire"
      }
    }
  ]
  default_rules = [
    {
      rulePriority = 998
      description  = "Expire untagged images older than ${var.untagged_ttl} days"
      selection = {
        tagStatus   = "untagged"
        countType   = "sinceImagePushed"
        countUnit   = "days"
        countNumber = var.untagged_ttl
      }
      action = {
        type = "expire"
      }
    },
    {
      rulePriority = 999
      description  = "Expire other images older than ${var.minimum_ttl} days"
      selection = {
        tagStatus   = "any"
        countType   = "sinceImagePushed"
        countUnit   = "days"
        countNumber = var.minimum_ttl
      }
      action = {
        type = "expire"
      }
    }
  ]
}